from flask import flash, render_template, abort

def handle_error_404(error):
	abort(404)


def handle_error_500(error):
	abort(404)


def crash():
	abort(500)

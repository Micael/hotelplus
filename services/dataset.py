from models import User
from models import Client
from models import Sejour
from models import Chambre
from models import Category
from models import Produit
from models import Commande
from models import CommandeDetail

users = [
    User("nom", "prenom", "telephone","email", "adresse", "login", "password","service", "active"),
    User("nom 2 ", "prenom 2 ", "telephone 2 ","email 2 ", "adresse 2 ", "login 2 ", "password 2 ","service 2 ", "active2 ")
]

clients = [
    Client("manuel", 'micael',"063577745", "0356181651612662", "15b boulevard georges")
]
categories = [
    Category("suite-senior", 60),
    Category("simple confort", 20),
    Category("moyen confort", 40),
    Category("suite junior", 50),
]
chambres = [
    Chambre(categories[0].serialize())
]

produits = [
    Produit("coca", 1),
    Produit("Sprite", 1),
    Produit("pepsi", 1),
]

commandes  = [
    Commande(clients[0].serialize(), "05/05/2019", 300)
]

commandeDetails = [
    CommandeDetail(commandes[0].serialize(), produits[0], 1 , 1),
    CommandeDetail(commandes[0].serialize(), produits[2], 3 , 1)
]
sejour = [
    Sejour(users[0].serialize(), "05/05/2019", "09/05/2019",clients[0].serialize(),chambres[0].serialize())
]
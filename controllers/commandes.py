from flask import jsonify, abort, make_response, request
import services.dataset as dataset
from models import Commande

commandes  = dataset.commandes

def getAll():
    if commandes:
        return jsonify(commandes=[commande.serialize() for commande in commandes])
    else:
        return abort(404)

def get(id):
    if len(commandes) >= id and id > 0:
        return jsonify(commandes[id - 1].serialize())
    else:
        return abort(404)

def delete(id):
    if len(commandes) >= id and id > 0:
        key = id - 1
        print(key)
        del commandes[key]
        resp = { "message": "commandes supprime !" }
        return make_response(jsonify(resp), 200)
    else:
        return abort(404)

def update(id):
    if len(commandes) >= id and id > 0:
        key = id - 1
        commande = commandes[key]

        return make_response(jsonify(commandes.serialize()), 200)
    else:
        return abort(404)
def create():
    commande =   Commande(clients[0].serialize(), "05/05/2019", 300)
    commandes.append(commande)
    return make_response(jsonify(commande.serialize()), 200)

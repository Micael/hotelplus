from flask import jsonify, abort, make_response, request
import services.dataset as dataset
from models import Client

clients  = dataset.clients

def getAll():
    if clients:
        return jsonify(clients=[client.serialize() for client in clients])
    else:
        return abort(404)

def get(id):
    if len(clients) >= id and id > 0:
        return jsonify(clients[id - 1].serialize())
    else:
        return abort(404)

def delete(id):
    if len(clients) >= id and id > 0:
        key = id - 1
        print(key)
        del clients[key]
        resp = { "message": "clients supprime !" }
        return make_response(jsonify(resp), 200)
    else:
        return abort(404)

def update(id):
    if len(clients) >= id and id > 0:
        key = id - 1
        client = clients[key]

        client.nom = request.form['nom'] 
        client.prenom = request.form['prenom'] 
        client.cb = request.form['cb'] 

        return make_response(jsonify(client.serialize()), 200)
    else:
        return abort(404)
def create():

    client = Client(
        request.form["nom"], 
        request.form["prenom"], 
        "telephone",
        request.form["cb"], 
        "adresse"
    )

    clients.append(client)
    return make_response(jsonify(client.serialize()), 200)

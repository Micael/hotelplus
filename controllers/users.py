from flask import jsonify, abort, make_response, request
import services.dataset as dataset
from models import User

users  = dataset.users

def getAll():
    if users:
        return jsonify(users=[user.serialize() for user in users])
    else:
        return abort(404)

def get(id):
    if len(users) > id and id > 0:
        return jsonify(users[id - 1].serialize())
    else:
        return abort(404)

def delete(id):
    if len(users) > id and id > 0:
        key = id - 1
        print(key)
        del users[key]
        resp = { "message": "Utilisateur supprime !" }
        return make_response(jsonify(resp), 200)
    else:
        return abort(404)

def update(id):
    if len(users) > id and id > 0:
        key = id - 1
        user = users[key]

        user.nom = request.form['nom'] 
        user.prenom = request.form['prenom'] 
        user.email = request.form['email'] 

        return make_response(jsonify(user.serialize()), 200)
    else:
        return abort(404)
def create():

    user = User(request.form["nom"], 
        request.form["prenom"], 
        "telephone",
        request.form["email"], 
        "adresse", 
        "login", 
        "password",
        "service",
        "active",
    )

    users.append(user)
    return make_response(jsonify(user.serialize()), 200)

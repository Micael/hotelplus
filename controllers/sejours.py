from flask import jsonify, abort, make_response, request
import services.dataset as dataset
from models import Sejour

sejours  = dataset.sejour
users = dataset.users
def getAll():
    if sejours:
        return jsonify(sejours=[sejour.serialize() for sejour in sejours])
    else:
        return abort(404)

def get(id):
    if len(sejours) >= id and id > 0:
        return jsonify(sejours[id - 1].serialize())
    else:
        return abort(404)

def delete(id):
    if len(sejours) >= id and id > 0:
        key = id - 1
        print(key)
        del sejours[key]
        resp = { "message": "sejours supprime !" }
        return make_response(jsonify(resp), 200)
    else:
        return abort(404)

def update(id):
    if len(sejours) >= id and id > 0:
        key = id - 1
        sejour = sejours[key]

        sejour.date_arrive = request.form['date_arrive']
        sejour.date_depart = request.form['date_depart'] 
        return make_response(jsonify(sejour.serialize()), 200)
    else:
        return abort(404)

def create():
    sejour = Sejour(users[0].serialize(), request.form['date_arrive'] , request.form['date_depart'] ,clients[0].serialize(),chambres[0])
    sejours.append(sejour)
    return make_response(jsonify(sejour.serialize()), 200)

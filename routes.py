import controllers.users as users
import controllers.clients as clients;
import controllers.sejours as sejours;
import controllers.commandes as commandes
import middlewares.errors as errors

# Fonction qui instancie les differentes Routes API
def get_routes_api(app):
    app.add_url_rule('/api/users', 'get_users', users.getAll, methods=['GET'])
    app.add_url_rule('/api/users/<int:id>', 'get_user', users.get, methods=['GET'])
    app.add_url_rule('/api/users', 'add_users', users.create, methods=['POST'])
    app.add_url_rule('/api/users/<int:id>', 'update_users', users.update, methods=['PUT'])
    app.add_url_rule('/api/users/<int:id>', 'delete_userx', users.delete, methods=['DELETE'])

    app.add_url_rule('/api/clients', 'get_clients', clients.getAll, methods=['GET'])
    app.add_url_rule('/api/clients/<int:id>', 'get_client', clients.get, methods=['GET'])
    app.add_url_rule('/api/clients', 'add_client', clients.create, methods=['POST'])
    app.add_url_rule('/api/clients/<int:id>', 'update_client', clients.update, methods=['PUT'])
    app.add_url_rule('/api/clients/<int:id>', 'delete_client', clients.delete, methods=['DELETE'])

    app.add_url_rule('/api/sejours', 'get_sejours', sejours.getAll, methods=['GET'])
    app.add_url_rule('/api/sejours/<int:id>', 'get_sejour', sejours.get, methods=['GET'])
    app.add_url_rule('/api/sejours', 'add_sejours', sejours.create, methods=['POST'])
    app.add_url_rule('/api/sejours/<int:id>', 'update_sejours', sejours.update, methods=['PUT'])
    app.add_url_rule('/api/sejours/<int:id>', 'delete_sejours', sejours.delete, methods=['DELETE'])

    app.add_url_rule('/api/commandes', 'get_commandes', commandes.getAll, methods=['GET'])
    app.add_url_rule('/api/commandes/<int:id>', 'get_commande', commandes.get, methods=['GET'])
    app.add_url_rule('/api/commandes', 'add_commandes', commandes.create, methods=['POST'])
    app.add_url_rule('/api/commandes/<int:id>', 'update_commandes', commandes.update, methods=['PUT'])
    app.add_url_rule('/api/commandes/<int:id>', 'delete_commandes', commandes.delete, methods=['DELETE'])
# Fonction qui instancie les differentes Routes d'erreur
# def init_error_handlers(app):
#     app.errorhandler(404)(errors.handle_error_404)
#     app.errorhandler(500)(errors.handle_error_500)

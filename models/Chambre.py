class Chambre:
    def __init__(self, category):
        self.category = category

    def serialize(self):
        return {
            "category": self.category
        }

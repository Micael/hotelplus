class Client:
    def __init__(self, nom, prenom, telephone, cb, adresse):
        self.nom = nom
        self.prenom = prenom 
        self.telephone = telephone
        self.cb = cb
        self.adresse = adresse
    def serialize(self):
        return {
            "nom": self.nom,
            "prenom": self.prenom,
            "telephone": self.telephone,
            "cb": self.cb,
            "adresse": self.adresse,
        }

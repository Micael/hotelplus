from enum import Enum

class Service(Enum):
    DIRECTION = 1     
    RECEPTION = 2     
    BAR = 3     
    COMPTABILITE = 4
class Sejour:
    def __init__(self, user, date_arrive, date_depart, client, chambre):
        self.user = user
        self.date_arrive = date_arrive
        self.date_depart = date_depart
        self.client = client
        self.chambre = chambre
    def serialize(self):
        return {
            "user": self.user,
            "date_arrive": self.date_arrive,
            "date_depart": self.date_depart,
            "client": self.client,
            "chambre": self.chambre,
        }

class Commande:
    def __init__(self, client, date_commande, prix_total):
        self.client = client
        self.date_commande = date_commande
        self.prix_total = prix_total

    def serialize(self):
        return {
            "client": self.client,
            "date_commande": self.date_commande,
            "prix_total" : self.prix_total
        }

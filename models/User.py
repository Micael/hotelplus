class User:
    def __init__(self, nom, prenom, telephone,email, adresse, login, password, service, active) :
        self.nom = nom 
        self.prenom = prenom
        self.telephone = telephone
        self.email = email
        self.adresse = adresse
        self.login = login
        self.password = password
        self.service = service
        self.active = active
    
    def serialize(self):
        return {
            "nom": self.nom,
            "prenom": self.prenom,
            "telephone": self.telephone,
            "email": self.email,
            "adresse": self.adresse,
            "login": self.login,
            "password": self.password,
            "service": self.service,
            "active": self.active
        }

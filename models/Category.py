class Category:
    def __init__(self, nom, prix):
        self.nom = nom
        self.prix = prix

    def serialize(self):
        return {
            "nom": self.nom,
            "prix": self.prix,
    }
